from django.contrib import admin
from companies.models import Company, EmployeeDetails, JobOpenings
# Register your models here.
class CompanyAdmin(admin.ModelAdmin):
    """
        Model Admin for Company
    """
    list_display = ('name', 'location',)


class EmployeeAdmin(admin.ModelAdmin):
    """
        Model admin for EmployeeDetails Model
    """
    list_display = ('name', 'role', 'age', 'company',)

class OpeningAdmin(admin.ModelAdmin):
    """
        Model admin for JobOpening Model
    """
    list_display = ('role', 'company',)

admin.site.register(Company, CompanyAdmin)
admin.site.register(EmployeeDetails, EmployeeAdmin)
admin.site.register(JobOpenings, OpeningAdmin)