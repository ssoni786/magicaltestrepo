from django.db import models

# Create your models here.
LOCATION_CHOICES = (('Mumbai', 'Mumbai'),('Delhi', 'Delhi'),
                    ('Bangalore', 'Bangalore'), ('Pune', 'Pune'))

ROLE_CHOICES = (('Developer', 'Developer'), ('Manager', 'Manager'),
                ('Project Lead', 'Project Lead'))

class Company(models.Model):
    """
        Model for storing company information
    """
    name = models.CharField(max_length=200)
    location = models.CharField(max_length=50, choices=LOCATION_CHOICES)


    def __unicode__(self):
        return self.name

class EmployeeDetails(models.Model):
    """
        Model for storing Employee details for company
    """
    name = models.CharField(max_length=100)
    age = models.PositiveSmallIntegerField()
    role = models.CharField(max_length=50, choices=ROLE_CHOICES)
    company = models.ForeignKey(Company, related_name='employee_list')

    def __unicode__(self):
        return self.name


class JobOpenings(models.Model):
    """
        Model for Job openings in companies.
    """
    role = models.CharField(max_length=50, choices=ROLE_CHOICES)
    company = models.ForeignKey(Company, related_name='job_openings')

