# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='no_of_employee',
        ),
        migrations.AlterField(
            model_name='employeedetails',
            name='company',
            field=models.ForeignKey(related_name='employee_list', to='companies.Company'),
        ),
    ]
