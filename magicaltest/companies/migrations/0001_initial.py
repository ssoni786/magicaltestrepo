# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('location', models.CharField(max_length=50, choices=[(b'Mumbai', b'Mumbai'), (b'Delhi', b'Delhi'), (b'Bangalore', b'Bangalore'), (b'Pune', b'Pune')])),
                ('no_of_employee', models.PositiveIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('age', models.PositiveSmallIntegerField()),
                ('role', models.CharField(max_length=50, choices=[(b'Developer', b'Developer'), (b'Manger', b'Manager'), (b'Project Lead', b'Project Lead')])),
                ('company', models.ForeignKey(related_name='employee_details', to='companies.Company')),
            ],
        ),
        migrations.CreateModel(
            name='JobOpenings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('role', models.CharField(max_length=50, choices=[(b'Developer', b'Developer'), (b'Manger', b'Manager'), (b'Project Lead', b'Project Lead')])),
                ('company', models.ForeignKey(related_name='job_openings', to='companies.Company')),
            ],
        ),
    ]
