"""------------This file contains serializers for Model---------"""
from rest_framework import serializers
from companies.models import Company, EmployeeDetails, JobOpenings

class EmployeeDetailsBasicSerializer(serializers.HyperlinkedModelSerializer):
    """
        Serializer for Employee details without company details
    """
    class Meta:
        model = EmployeeDetails
        fields = ('url', 'id', 'name', 'age', 'role')
        depth = 1

class EmployeeDetailsSerializer(EmployeeDetailsBasicSerializer):
    """
        Serializer for Employee details with company details
    """
    class Meta(EmployeeDetailsBasicSerializer.Meta):
        fields = ('url', 'id', 'name', 'age', 'role', 'company')
        depth = 1

class JobOpeningBasicSerializer(serializers.HyperlinkedModelSerializer):
    """
        Serializer for JobOpenings model without company details
    """

    class Meta:
        model = JobOpenings
        fields = ('url', 'id', 'role',)

class JobOpeningSerializer(JobOpeningBasicSerializer):
    """
        Serializer for JobOpenings model with company details
    """
    class Meta(JobOpeningBasicSerializer.Meta):
        fields = ('url', 'id','role', 'company')
        depth = 1

class CompanySerializer(serializers.HyperlinkedModelSerializer):
    """
        Serializer for Company Model
    """
    no_of_employee = serializers.SerializerMethodField()
    employee_list = EmployeeDetailsBasicSerializer(many=True)
    job_openings = JobOpeningBasicSerializer(many=True)

    class Meta:
        model = Company
        fields = ('url', 'id', 'name', 'location', 'no_of_employee', 'employee_list', 'job_openings')

    def get_no_of_employee(self, obj):
        return obj.employee_list.all().count()


