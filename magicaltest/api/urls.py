from django.conf.urls import include, url
from rest_framework import routers
from api import views
router = routers.DefaultRouter()
router.register(r'companies', views.CompanyViewSet)
router.register(r'employees', views.EmployeeViewSet)
router.register(r'jobopenings', views.JobOpeningViewSet)
router.register(r'searchapi', views.SearchViewSet, 'search')


urlpatterns = [
    url(r'^', include(router.urls)),


]