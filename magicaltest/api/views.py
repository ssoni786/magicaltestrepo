from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.views import Response, status
from api.serializers import CompanySerializer, EmployeeDetailsSerializer, JobOpeningSerializer
from companies.models import Company, EmployeeDetails, JobOpenings
# Create your views here.

class CompanyViewSet(viewsets.ModelViewSet):
    """
        Api endpoints for companies
    """
    queryset = Company.objects.all().order_by('-id')

    serializer_class = CompanySerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    """
        Api endpoint for EmployeeDetails
    """
    queryset = EmployeeDetails.objects.all().order_by('-id')
    serializer_class = EmployeeDetailsSerializer


class JobOpeningViewSet(viewsets.ModelViewSet):
    """
        Api endpoint for JobOpenings
    """
    queryset = JobOpenings.objects.all().order_by('-id')
    serializer_class = JobOpeningSerializer


class SearchViewSet(viewsets.ViewSet):
    """
        Api endpoint for searching Employee and Job openings
    """

    def list(self, request):
        search = request.GET.get('search', None)
        employee_objs = EmployeeDetails.objects.all()
        companies_objs = Company.objects.all()
        if search:
            search = search.strip()
            employee_objs = employee_objs.filter(role__istartswith=search)
            companies_objs = companies_objs.filter(job_openings__role__istartswith=search).distinct()
        employees_data = EmployeeDetailsSerializer(instance=employee_objs, many=True, context={'request': request}).data
        companies_data = CompanySerializer(instance=companies_objs, many=True, context={'request': request}).data
        return Response({'companies': companies_data, 'employees': employees_data}, status=status.HTTP_200_OK)
